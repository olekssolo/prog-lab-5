public class WildPickUp4Card implements Wild {
    public boolean canPlay(Card card) {
        return true;
    }
    public String newColor(String choice) {
        return "new color: "+ choice + " and +4 to next player";
    }
    public String getColor() {
        return "wild";
    }
    public int getNumber() {
        return -1;
    }
}
