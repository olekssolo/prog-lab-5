public interface Wild extends Card {
    public boolean canPlay(Card card);
    public String getColor();
    public String newColor(String choice);
}
