public class NumberedCard implements Card {
    protected int number;
    protected String color;

    public NumberedCard (int number, String color) {
        this.number = number;
        this.color = color;
    }
    public boolean canPlay(Card card) {
        if (card.getNumber() == this.number || card.getColor() == this.color) {
            return true;
        }
        else if (card.getColor() == "wild") {
            return true;
        }
        else {
            return false;
        }
    }
    public String getColor() {
        return this.color;
    }
    public int getNumber() {
        return this.number;
    }
}