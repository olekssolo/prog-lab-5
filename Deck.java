import java.util.ArrayList;
import java.util.Random;

public class Deck {
    protected ArrayList<Card> deck;
    String[] colors = {"red", "blue", "green", "yellow"};
    Random rand = new Random();

    public Deck() {
        for (int i=0; i<4; i++) {
            WildCard wild = new WildCard();
            deck.add(wild);
            WildPickUp4Card wild4 = new WildPickUp4Card();
            deck.add(wild4);
        }
        for (int j=0; j<2; j++) {
            for (int k=0; k<10; k++) {
                for (int l=0; l<colors.length; l++) {
                    NumberedCard card = new NumberedCard(k, colors[l]);
                    deck.add(card);
                }
            }
        }
        for (int m=0; m<2; m++) {
            for (int n=0; n<colors.length; n++) {
                PickUp2Card pickup2 = new PickUp2Card(colors[n]);
                ReverseCard reverse = new ReverseCard(colors[n]);
                SkipCard skip = new SkipCard(colors[n]);
                deck.add(pickup2);
                deck.add(reverse);
                deck.add(skip);
            }
        }
    }
    public void addToDeck(Card newcard) {
        deck.add(newcard);
    }
    public Card draw() {
        Card topcard = deck.get(0);
        deck.remove(0);
        return topcard;
    }
    public void shuffle() {
        for (int i=0; i<deck.size(); i++) {
            int random = rand.nextInt();
            Card holdThis = deck.get(i);
            deck.set(i, deck.get(random));
            deck.set(random, holdThis);
        }
    }
}
