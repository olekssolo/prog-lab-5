public class PickUp2Card implements Card {
    protected String color;

    public PickUp2Card(String color) {
        this.color = color;
    }
    public boolean canPlay(Card card) {
        if (card instanceof PickUp2Card == true || card.getColor() == this.color) {
            return true;
        }
        else if (card.getColor() == "wild") {
            return true;
        }
        else {
            return false;
        }
    }
    public String pickUpTwo() {
        return "next player, pick up 2";
    }
    public String getColor() {
        return this.color;
    }
    public int getNumber() {
        return -1;
    }
}
