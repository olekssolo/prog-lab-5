public class SkipCard implements Card {
    protected String color;

    public SkipCard (String color) {
        this.color = color;
    }
    public boolean canPlay(Card card) {
        if (card instanceof SkipCard == true || card.getColor() == this.color) {
            return true;
        }
        else if (card.getColor() == "wild") {
            return true;
        }
        else {
            return false;
        }
    }
    public String skip() {
        return "next player's turn skipped";
    }
    public String getColor() {
        return this.color;
    }
    public int getNumber() {
        return -1;
    }
}
