public class ReverseCard implements Card {
    protected String color;

    public ReverseCard (String color) {
        this.color = color;
    }
    public boolean canPlay(Card card) {
        if (card instanceof ReverseCard == true || card.getColor() == this.color) {
            return true;
        }
        else if (card.getColor() == "wild") {
            return true;
        }
        else {
            return false;
        }
    }
    public String reverse() {
        return "changing directions";
    }
    public String getColor() {
        return this.color;
    }
    public int getNumber() {
        return -1;
    }
}
