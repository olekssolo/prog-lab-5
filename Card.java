public interface Card {
    public boolean canPlay(Card card);
    public String getColor();
    public int getNumber();
}
