public class WildCard implements Wild {
    public boolean canPlay(Card card) {
        return true;
    }
    public String newColor(String choice) {
        return "new color chosen: "+choice;
    }
    public String getColor() {
        return "wild";
    }
    public int getNumber() {
        return -1;
    }
}
