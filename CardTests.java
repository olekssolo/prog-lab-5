import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class CardTests {
    @Test
    public void getColorTest() {
        NumberedCard number = new NumberedCard(5, "red");
        assertEquals("red", number.getColor());
        PickUp2Card pickup = new PickUp2Card("yellow");
        assertEquals("yellow", pickup.getColor());
        ReverseCard reverse = new ReverseCard("blue");
        assertEquals("blue", reverse.getColor());
        SkipCard skip = new SkipCard("green");
        assertEquals("green", skip.getColor());
    }
    @Test
    public void getNumberTest() {
        NumberedCard number = new NumberedCard(5, "red");
        assertEquals(5, number.getNumber());
    }
    @Test
    public void canPlayTest() {
        NumberedCard number = new NumberedCard(2, "yellow");
        NumberedCard number2 = new NumberedCard(6, "yellow");
        NumberedCard number3 = new NumberedCard(2, "red");
        NumberedCard number4 = new NumberedCard(5, "blue");
        PickUp2Card pickup = new PickUp2Card("yellow");
        SkipCard skip = new SkipCard("red");
        WildCard wild = new WildCard();
        assertEquals(true, number.canPlay(pickup));
        assertEquals(false, number.canPlay(skip));
        assertEquals(true, number.canPlay(wild));
        assertEquals(true, number.canPlay(number2));
        assertEquals(true, number.canPlay(number3));
        assertEquals(false, number.canPlay(number4));
    }
}
